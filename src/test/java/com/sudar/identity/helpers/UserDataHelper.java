package com.sudar.identity.helpers;

import com.sudar.identity.entity.User;

import java.util.Arrays;

/**
 * Created by pillis on 08/02/2017.
 */
public class UserDataHelper {

    public User createRandomUser() {
        return User.builder()
                .title("Mr")
                .initial("Pilli")
                .firstName("sudar")
                .secondName(null)
                .email_id("sudar@gmail.com")
                .address(Arrays.asList(User.Address.builder()
                        .first_Lane("first lane")
                        .second_Lane("second lane")
                        .town("town")
                        .city("city")
                        .country("uk")
                        .postCode("AB12 3EF")
                        .build()))
                .build();
    }
}
