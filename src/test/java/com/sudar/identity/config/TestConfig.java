package com.sudar.identity.config;

import com.sudar.identity.helpers.UserDataHelper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by pillis on 08/02/2017.
 */
@Configuration
public class TestConfig {

    @Bean
    public UserDataHelper userDataHelper() {
        return new UserDataHelper();
    }
}
