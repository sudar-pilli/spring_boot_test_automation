package com.sudar.identity;

import com.sudar.identity.entity.User;
import com.sudar.identity.helpers.UserDataHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pillis on 02/02/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UserControllerIT {
    @LocalServerPort
    private int port;
    private URL base;

    @Autowired
    private TestRestTemplate template;
    @Autowired
    private UserDataHelper userDataHelper;

    @Before
    public void setUp() throws Exception {
        this.base = new URL("http://localhost:" + port + "/");
    }

    @Test
    public void testRootController() throws Exception {
        ResponseEntity<String> response = template.getForEntity(base.toString(),
                String.class);
        assertThat(response.getBody()).as("").isEqualTo("Greetings from Spring Boot!");
    }

    @Test
    public void successFullyCreateUser() {
        User user = userDataHelper.createRandomUser();
        ResponseEntity<User> response = template.postForEntity(base.toString().concat("/user"), user, User.class);

        assertThat(response.getStatusCode()).as("user response code").isEqualTo(HttpStatus.CREATED);
    }
}
