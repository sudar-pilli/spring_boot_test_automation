package com.sudar.identity.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.List;

/**
 * Created by pillis on 02/02/2017.
 */
@Component
@Entity
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String title;
    String firstName;
    String secondName;
    String initial;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
    List<Address> address;
    String email_id;

    @FieldDefaults(level = AccessLevel.PRIVATE)
    @Data
    @Builder
    @Entity
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Address {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        int id;
        @ManyToOne
        User user;
        String first_Lane;
        String second_Lane;
        String town;
        String city;
        String country;
        String postCode;
    }
}
