package com.sudar.identity.utils;

import lombok.*;
import lombok.experimental.FieldDefaults;

/**
 * Created by pillis on 02/02/2017.
 */
@FieldDefaults(level = AccessLevel.PRIVATE)
@Data
@NoArgsConstructor
public class UserUtil {

}
