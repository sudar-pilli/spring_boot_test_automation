package com.sudar.identity.controller.user;

/**
 * Created by pillis on 02/02/2017.
 */

import com.sudar.identity.entity.User;
import com.sudar.identity.service.UserService;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Optional;

@FieldDefaults(level = AccessLevel.PRIVATE)
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;
    User user;

    @PostMapping(produces="application/json",consumes="application/json")
    @ResponseBody
    public User createUser(@RequestBody User user, HttpServletResponse response) {
        Optional.of(userService.validateUser(Optional.ofNullable(user)))
                .filter(b -> b.booleanValue())
                .map(flag -> {
                    this.user = userService.persistUserDetails(user);
                    response.setStatus(HttpStatus.CREATED.value());
                    return flag;
                })
                .orElseGet(() -> {
                    response.setStatus(HttpStatus.NOT_ACCEPTABLE.value());
                    return false;
                });
        return this.user;
    }

//    @PostMapping(produces="application/json",consumes="application/json")
//    @ResponseBody
//    public User getUserByEmail_Id(@RequestBody String emailId, HttpServletResponse response) {
//        userService.getUserDetailsByEmail_Id(Optional.ofNullable(emailId))
//                .ifPresent(() -> {
//
//                });
//        response.setStatus(HttpStatus.CREATED.value());
//        return user;
//    }
}
