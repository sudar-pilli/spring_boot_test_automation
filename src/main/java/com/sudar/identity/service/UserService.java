package com.sudar.identity.service;

import com.sudar.identity.entity.User;
import com.sudar.identity.repository.UserRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by pillis on 02/02/2017.
 */

@FieldDefaults(level = AccessLevel.PRIVATE)
@Builder
@Data
@AllArgsConstructor
@Component
public class UserService {
    User user;
    @Autowired
    UserRepository userRepository;

    public boolean validateUser(Optional<User> userModelOptional) {
        this.user = userModelOptional.orElseThrow(() -> new RuntimeException("user can't be null"));
        return  verifyCountry().
                and(verifyEmail_Id()).
                and(verifyFirstName())
                .test(user);
    }

    public Optional<User> getUserDetailsByEmail_Id(Optional<String> emailId) {
        emailId
                .filter(email -> !StringUtils.isEmpty(email))
                .orElseThrow(() -> new RuntimeException("user email address can't be null"));

        return userRepository.findAll().stream()
                .filter(user -> user.getEmail_id().equalsIgnoreCase(emailId.get()))
                .findAny();
    }

    public User persistUserDetails(User user) {
        return userRepository.saveAndFlush(user);
    }

    private Predicate<User> verifyCountry() {
        return p -> !StringUtils.isEmpty(p.getAddress().stream().findFirst().get()) &&
                !StringUtils.isEmpty(p.getAddress().stream().findFirst().get().getCountry()) &&
                p.getAddress().stream().findFirst().get().getCountry().equalsIgnoreCase("GB");
    }

    private Predicate<User> verifyEmail_Id() {
        return p -> !StringUtils.isEmpty(p.getEmail_id())
                && p.getEmail_id().contains("@");
    }

    private Predicate<User> verifyFirstName() {
        return p -> !StringUtils.isEmpty(p.getFirstName());
    }
}
